const { readFile, readdirSync } = require('fs');
const { request } = require('https');
const { spawn } = require('child_process');
const moment = require('moment');
const { parseZone } = require('moment');
const { Observable, Subscriber, of, throwError } = require('rxjs');
const { isNumber, isString } = require('lodash');

const { uriSignature } = require('./ifvod');

/**
 * read file as JSON object
 * @param {string} readPath path to file
 * @returns {Observable<string>} Observable of JSON object
 */
function readJsonFile(readPath) {
  return new Observable((subscriber$) => {
    readFile(readPath, 'utf8', (err, data) => {
      if (err) {
        subscriber$.error(err);
        subscriber$.complete();
        return;
      }
      try {
        subscriber$.next(JSON.parse(data));
      } catch (parseJsonErr) {
        subscriber$.error(parseJsonErr);
      }
      subscriber$.complete();
    });
  });
}

/**
 * extract key from URL
 * @param {string} url URL that contains the "id" param
 * @returns {string} key
 */
function extractKeyFromUrl(url) {
  const [_, __, captureGroupA, ____, captureGroupB] = url.match(
    /(id=(.*?)&)|(id=(.*))/
  );
  const key = captureGroupA || captureGroupB;
  return !isString(key) || key.trim().length === 0 ? null : key;
}

/**
 * generate sub-directory name from subscription name
 * @param {string} fileName Subscription file name
 * @returns {string} sub-directory name
 */
function generateSubDirectoryName(fileName) {
  const [_, subDirectoryName] = fileName.match(/^(.*)\./);
  return !isString(subDirectoryName) || subDirectoryName.trim().length === 0
    ? null
    : subDirectoryName;
}

/**
 * determine if a video should be kept
 * @type {function(SubscriptionFileContent, VideoRecord): *|boolean}
 */
const keepVideoRecord = (function (today) {
  return function ({ filterByLastNumberOfDays }, { updateDate }) {
    return isNumber(filterByLastNumberOfDays) && filterByLastNumberOfDays > 1
      ? parseZone(updateDate)
          .add(filterByLastNumberOfDays, 'days')
          .isAfter(today)
      : true;
  };
})(moment().startOf('d'));

/**
 * list subscription files, emit sequentially
 * @param {string} readPath path to directory containing subscription files
 * @returns {Observable<module:fs.Dirent>} Observable of subscription files
 */
function listSubscriptionFiles(readPath) {
  try {
    /**
     * @type {module:fs.Dirent[]}
     */
    const filePaths = readdirSync(readPath, {
      withFileTypes: true,
    });
    /**
     * @desc emit subscription files sequentially
     */
    return of(...filePaths.filter(({ name }) => /.*.json$/i.test(name)));
  } catch (err) {
    return throwError(err);
  }
}

/**
 * make a HTTP GET request
 * @param {Subscriber} subscriber$ The subject to emit results through
 * @param {ClientRequestArgs} options Http request options
 * @returns {undefined}
 */
function httpGet(subscriber$, options) {
  let [path, queryString] = options.path.split(/(\?.*)/);

  path = queryString ? `${path}${uriSignature(queryString)}` : path;

  const req = request(
    {
      ...options,
      path,
    },
    (res) => {
      res.setEncoding('utf8');

      const data = [];
      res.on('data', (chunk) => data.push(chunk));
      res.on('end', () => {
        try {
          subscriber$.next(JSON.parse(data.join('')));
          subscriber$.complete();
        } catch (err) {
          subscriber$.error(err);
          subscriber$.complete();
        }
      });
      res.on('error', (err) => {
        subscriber$.error(err);
        subscriber$.complete();
      });
    }
  );

  req.on('error', (err) => {
    subscriber$.error(err);
    subscriber$.complete();
  });

  req.end();
}

/**
 * spawn a ffmpeg instance as a child process to receive HLS chunks and convert to video file
 * @param {Subscriber} subscriber$ The subject to emit results through
 * @param {string} ffmpegBinPath The path to the ffmpeg binary
 * @param {string} url The URL to the HLS stream's .m3u8 playlist
 * @param {string} writePath The output path for the video file
 * @returns {void} N/A
 */
function ffmpegSaveHls(subscriber$, ffmpegBinPath, url, writePath) {
  const cmd =
    'ffmpeg -loglevel quiet -protocol_whitelist file,http,https,tcp,tls,crypto';
  const [_, ...args] = cmd.split(' ');

  let isComplete = false;
  // note - use `spawn` to not accumulate stderr / stdout
  const childProcess = spawn(ffmpegBinPath, [
    ...args,
    '-i',
    url,
    '-c',
    // pass through all media channels
    'copy',
    // overwrite output file if already exist
    '-y',
    writePath,
  ]);
  childProcess.on('exit', (exitCode) => {
    if (isComplete) {
      return;
    }
    isComplete = true;
    if (exitCode === 0) {
      subscriber$.next();
    } else {
      subscriber$.error(`ffmpeg exited with code: ${exitCode}`);
    }
    subscriber$.complete();
  });
  childProcess.on('error', (err) => {
    if (isComplete) {
      return;
    }
    isComplete = true;
    subscriber$.error(err);
    subscriber$.complete();
  });
}

module.exports = {
  keepVideoRecord,
  listSubscriptionFiles,
  httpGet,
  ffmpegSaveHls,
  readJsonFile,
  extractKeyFromUrl,
  generateSubDirectoryName,
};
