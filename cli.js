#!/usr/bin/env node

'use strict';
const {
  unlinkSync,
  mkdirSync,
  existsSync,
  writeFileSync,
  readFileSync,
} = require('fs');
const { join } = require('path');
const process = require('process');
const findProcess = require('find-process/lib/find_process');
const { of, Observable, throwError } = require('rxjs');
const { concatMap, switchMap, map, catchError } = require('rxjs/operators');
const moment = require('moment');
const { isPlainObject, assign } = require('lodash');
/**
 * @type {{i: string, d: string, q: string, D: string, F: string}}
 */
const argv = require('yargs')
  .usage('Usage: $0 [options]')
  .alias('d', 'directory')
  .nargs('d', 1)
  .string('d')
  .describe('d', 'Specify the working directory, defaults to cwd')
  .alias('i', 'init')
  .nargs('i', 0)
  .boolean('i')
  .describe('i', 'Initialize the working directory')
  .alias('D', 'download-directory')
  .nargs('D', 1)
  .string('D')
  .describe(
    'D',
    'Specify the download directory, defaults to downloads folder under the working directory'
  )
  .alias('F', 'ffmpeg-bin')
  .nargs('F', 1)
  .string('F')
  .describe('F', 'Specify the path to ffmpeg binary')
  .alias('q', 'quiet')
  .nargs('q', 0)
  .boolean('q')
  .describe('q', 'Do not output to stdout or stderr')
  .help('h')
  .alias('h', 'help').argv;

const { path } = require('./const');
const {
  keepVideoRecord,
  listSubscriptionFiles,
  readJsonFile,
  extractKeyFromUrl,
  generateSubDirectoryName,
  ffmpegSaveHls,
} = require('./utils');
const { fetchThread } = require('./api/thread/api');
// const { fetchThread } = require('./mock-api/thread/api');
const { fetchVideoRecordDetail } = require('./api/video-record-detail/api');
// const { fetchVideoRecordDetail } = require('./mock-api/video-record-detail/api');

const cwd = argv.d || process.cwd();
const downloadPath = argv.D || join(cwd, path.downloads);
const lockPath = join(cwd, path.lock);

/**
 * log error message
 * @param {Error|string} err Error message
 * @returns {undefined}
 */
function logError(err) {
  if (argv.q !== true) {
    console.error(err);
  }
}

/**
 * log message
 * @param {string} msg Message
 * @returns {undefined}
 */
function log(msg) {
  if (argv.q !== true) {
    console.log(msg);
  }
}

/**
 * init the current working directory
 * - create downloads folder, if not already exist
 * - create subscriptions folder, if not already exist
 * - create sample subscription file
 * - create account credentials file
 * @returns {Error|*} Error, if any
 */
function init() {
  // create the "downloads" folder
  let writePath = downloadPath;
  if (!existsSync(writePath)) {
    try {
      mkdirSync(writePath);
    } catch (err) {
      return err;
    }
  }

  // create the "subscriptions" folder
  writePath = join(cwd, path.subscriptions);
  if (!existsSync(writePath)) {
    try {
      mkdirSync(writePath);
    } catch (err) {
      return err;
    }
  }

  // create a subscription sample file
  writePath = join(cwd, path.subscriptions, path.subscriptionSample);
  const subscriptionSample = {
    url: 'https://www.ifvod.tv/detail?id=<key>',
    filterByLastNumberOfDays: 31,
  };
  try {
    writeFileSync(writePath, JSON.stringify(subscriptionSample, null, 2));
  } catch (err) {
    return err;
  }
}

/**
 * remove expired video from the sub-directory
 * @param {string} fileName Subscription file name
 * @param {string} filePath The file path to subscription file
 * @returns {Observable<SubscriptionFileContent>} updated subscription file content
 */
function removeExpiredVideos(fileName, filePath) {
  const subDirectoryName = generateSubDirectoryName(fileName);
  return readJsonFile(filePath).pipe(
    map((fileContent) =>
      Object.entries(fileContent).reduce(
        ([hasUpdate, acc], [key, val]) => {
          // if the value is a nested object - a VideoRecord object and the video has expired
          if (isPlainObject(val) && !keepVideoRecord(fileContent, val)) {
            const videoRecord = val;
            const writePath = join(
              downloadPath,
              subDirectoryName,
              `${videoRecord.name}.mp4`
            );
            // if the corresponding video file does not exist
            if (!existsSync(writePath)) {
              return [hasUpdate || true, acc];
            }
            // if the corresponding video file is removed successfully
            try {
              unlinkSync(writePath);
              return [hasUpdate || true, acc];
            } catch (err) {
              // if the corresponding video file cannot be removed
              logError(
                `Unable to remove expired video "${writePath}". Error: ${err}`
              );
              return [hasUpdate, assign(acc, { [key]: val })];
            }
          }
          // if the value is not a nested object
          return [hasUpdate, assign(acc, { [key]: val })];
        },
        [false, {}]
      )
    ),
    switchMap(([hasUpdate, fileContent]) => {
      if (!hasUpdate) {
        return of(fileContent);
      }
      // if the subscription file is updated successfully
      try {
        writeFileSync(filePath, JSON.stringify(fileContent, null, 2));
        return of(fileContent);
      } catch (err) {
        // if the subscription file cannot be updated
        logError(`Unable to update subscription "${filePath}". Error: ${err}`);
        return of();
      }
    })
  );
}

/**
 * fetch and save a HLS stream as a video file
 * @param {string} url The URL to the HLS stream's .m3u8 playlist
 * @param {string} writePath The output path for the video file
 * @returns {Observable<void>} fetchResponse$ The response of video stream fetching and saving
 */
function fetchAndSaveHls(url, writePath) {
  return new Observable((subscriber$) =>
    ffmpegSaveHls(subscriber$, argv.F, url, writePath)
  );
}

/**
 * download and save video under a sub-directory of the download directory
 * @param {string} subDirectoryName The name of the sub-directory
 * @param {VideoRecord} videoRecord The video record to fetch from
 * @returns {Observable<void>} fetchResponse$ The response of video stream fetching and saving
 */
function downloadAndSaveVideo(subDirectoryName, { key, name }) {
  const subDirectoryPath = join(downloadPath, subDirectoryName);
  if (!existsSync(subDirectoryPath)) {
    try {
      mkdirSync(subDirectoryPath);
    } catch (err) {
      return throwError(
        `Unable to create sub-directory "${subDirectoryPath}". Error: ${err}`
      );
    }
  }

  const writePath = join(subDirectoryPath, `${name}.mp4`);
  return fetchVideoRecordDetail(key).pipe(
    map(({ data: { info: [{ flvPathList }] } }) =>
      flvPathList.find(({ isHls }) => isHls)
    ),
    switchMap((videoStream) =>
      videoStream ? fetchAndSaveHls(videoStream.result, writePath) : of()
    )
  );
}

/**
 * download videos from a thread page as a guest
 * @param {string} fileName Subscription file's name
 * @param {SubscriptionFileContent} fileContent Subscription File Content
 * @returns {Observable<VideoRecord>} Downloaded attachment
 */
function downloadVideosAsGuest(fileName, fileContent) {
  const { url } = fileContent;
  const key = extractKeyFromUrl(url);
  const subDirectoryName = generateSubDirectoryName(fileName);

  if (!key || !subDirectoryName) {
    return of();
  }

  /**
   * @type {Observable<VideoRecord>}
   */
  const videoRecord$ = fetchThread(key).pipe(
    switchMap(
      ({
        data: {
          info: [{ guestSeriesList }],
        },
      }) => {
        // video records that are within range and not already downloaded
        const videoRecords = guestSeriesList.filter(
          (videoRecord) =>
            keepVideoRecord(fileContent, videoRecord) &&
            !fileContent[`${videoRecord.id}`]
        );
        if (videoRecords.length === 0) {
          log('  video records are up to date');
        } else {
          log('  downloading videos...');
        }
        return of(...videoRecords);
      }
    ),
    catchError((err) => {
      logError(`Unable to fetch thread "${key}". Error: ${err}`);
      return of();
    })
  );
  return videoRecord$.pipe(
    // iterate through video records sequentially
    concatMap((videoRecord) =>
      downloadAndSaveVideo(subDirectoryName, videoRecord).pipe(
        map(() => {
          log(`  video: "${videoRecord.name}" downloaded`);
          return videoRecord;
        })
      )
    )
  );
}

/**
 * download from the latest video records under each thread
 * @returns {void} N/A
 */
function main() {
  const thread$ = listSubscriptionFiles(join(cwd, path.subscriptions));
  const subscription = thread$
    .pipe(
      // iterate through subscription files sequentially
      concatMap(({ name }) => {
        const filePath = join(cwd, path.subscriptions, name);
        return removeExpiredVideos(name, filePath).pipe(
          switchMap((fileContent) => {
            log(`thread: "${name}"`);
            return downloadVideosAsGuest(name, fileContent);
          }),
          map((videoRecord) => [filePath, videoRecord])
        );
      })
    )
    .subscribe(([filePath, videoRecord]) => {
      // if the subscription file is updated successfully
      try {
        const fileContent = JSON.parse(readFileSync(filePath, 'utf8'));
        const { id, key, name, updateDate } = videoRecord;
        fileContent[videoRecord.id] = {
          id,
          key,
          name,
          updateDate,
        };
        writeFileSync(filePath, JSON.stringify(fileContent, null, 2));
      } catch (err) {
        // if the subscription file cannot be updated
        logError(`Unable to update subscription "${filePath}". Error: ${err}`);
      }
    });

  process.on('exit', () => {
    subscription.unsubscribe();
    unlinkSync(lockPath);
  });
}

/**
 * bootstrap
 * @returns {Promise<void>} N/A
 */
async function bootstrap() {
  // if another instance is already running at the working directory
  if (existsSync(lockPath)) {
    // if the lock file is read successfully
    try {
      const { pid, startAt } = JSON.parse(readFileSync(lockPath, 'utf8'));
      const [processMatch] = await findProcess({ pid });

      // if the previous instance is still running
      if (processMatch) {
        logError(
          `Another instance (pid: ${pid}) is already running at directory "${cwd}", and it started at ${startAt}.`
        );
        process.exit(1);
      }
    } catch (err) {
      // if the lock file cannot be read
      logError(`Unable to read lock file "${lockPath}". Error: ${err}`);
      process.exit(1);
    }

    // if the previous instance is no longer running
    try {
      unlinkSync(lockPath);
    } catch (err) {
      // if the previous instance's lock file cannot be removed
      logError(
        `Unable to delete lock file that belongs to a previous instance "${lockPath}". Error: ${err}`
      );
      process.exit(1);
    }
  }

  /**
   * @type {LockFileContent}
   */
  const lock = {
    pid: process.pid,
    startedAt: moment().format(),
  };
  // if the lock file is written successfully
  try {
    writeFileSync(lockPath, JSON.stringify(lock, null, 2));
  } catch (err) {
    // if the lock file cannot be written
    logError(`Unable to set lock file "${lockPath}". Error: ${err}`);
    process.exit(1);
  }

  // if init
  if (argv.i) {
    const errInit = init();
    if (errInit) {
      logError(`Unable to initialize. Error: ${errInit}`);
      process.exit(1);
    }
    process.exit(0);
  }

  // if download directory is invalid
  if (!existsSync(downloadPath)) {
    logError(`Directory "${downloadPath}" does not exist.`);
    process.exit(1);
  }

  // if the path to ffmpeg binary is not specified or the path is invalid
  if (!argv.F || !existsSync(argv.F)) {
    logError(`Path to ffmpeg "${argv.F}" does not exist.`);
    process.exit(1);
  }

  main();
}

// if cwd is invalid
if (!existsSync(cwd)) {
  logError(`Directory "${cwd}" does not exist.`);
  process.exit(1);
}

bootstrap();
