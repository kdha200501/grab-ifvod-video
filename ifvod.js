/* eslint-disable */
const { Md5 } = require('ts-md5/dist/md5');

const privateKey = [
  'version001',
  'vers1on001',
  'vers1on00i',
  'bersion001',
  'vcrsion001',
  'versi0n001',
  'versio_001',
  'version0o1',
];

/**
 * generate private key for a public key
 * @param {number} publicKey A timestamp
 * @returns {string} Private key
 */
function getPrivateKey(publicKey) {
  return privateKey[publicKey % privateKey.length];
}

/**
 * not sure what this does, too bad there's no source map available
 * @param {string} e Query string
 * @returns {string} Query string
 */
function get_query(e) {
  let t = '';
  if (e.indexOf('?') > -1) {
    var n = e.substring(e.indexOf('?') + 1).split('&');
    if (n.length > 0) {
      for (var i = 0; i < n.length; i++) {
        n[i] = n[i].split('='), t += n[i][0];
        for (var r = 1; r < n[i].length; ++r) {
          t += `=${decodeURIComponent(n[i][r]).split('+').join(' ')}`;
        }
        t += '&';
      }
    }
  }
  return t != '' && (t = t.substring(0, t.length - 1)), t;
}

/**
 * append validation params to query string
 * @param {string} t Query string
 * @returns {string} Query string with validation params
 */
function uriSignature(t) {
  const publicKey = new Date().getTime();
  const i = new Md5();
  let r = get_query(t);
  r = r.toLowerCase();
  r = `${publicKey}&${r}&${getPrivateKey(publicKey)}`;
  const vv = i.appendStr(r).end();

  return `${t}&vv=${vv}&pub=${publicKey}`;
}

module.exports = {
  uriSignature,
};
