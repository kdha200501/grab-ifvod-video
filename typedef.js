/**
 * @typedef {{startedAt: string, pid: number}} LockFileContent
 */

/**
 * @typedef {{url: string, filterByLastNumberOfDays: number} | Object.<number, VideoRecord>} SubscriptionFileContent
 */

/**
 * @typedef {{id: number, key: string, name: string, updateDate: string}} VideoRecord
 */

/**
 * @typedef {{guestSeriesList: VideoRecord[], vipSeriesList: VideoRecord[]}} Thread
 */

/**
 * @typedef {{data: {info: Thread[]}}} ThreadResponse
 */

/**
 * @typedef {{isHls: boolean, result: string}} VideoStream
 */

/**
 * @typedef {{flvPathList: VideoStream[]}} VideoRecordDetail
 */

/**
 * @typedef {{data: {info: VideoRecordDetail[]}}} VideoRecordDetailResponse
 */
