const { Observable } = require('rxjs');

const {
  defaultProtocol,
  defaultHostname,
  defaultPort,
} = require('../../const');
const { httpGet } = require('../../utils');

const apiEndpoint = '/api/video/play?cinema=1&region=CA&device=1&ispath=true';

/**
 * request video-record-detail from API
 * @param {string} key Thread ID
 * @returns {Observable<VideoRecordDetailResponse>} API response for the requested thread
 */
function fetchVideoRecordDetail(key) {
  const path = `${apiEndpoint}&id=${key}`;
  /**
   * @type {ClientRequestArgs} options Http request options
   */
  const options = {
    protocol: `${defaultProtocol}:`,
    hostname: defaultHostname,
    port: defaultPort,
    path,
    method: 'GET',
  };

  return new Observable((subscriber$) => httpGet(subscriber$, options));
}

module.exports = {
  fetchVideoRecordDetail,
};
