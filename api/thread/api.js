const { Observable } = require('rxjs');

const {
  defaultProtocol,
  defaultHostname,
  defaultPort,
} = require('../../const');
const { httpGet } = require('../../utils');

const apiEndpoint =
  '/api/video/detail?cinema=1&device=1&player=CkPlayer&tech=HLS&country=HU&lang=cns&v=1';

/**
 * request thread from API
 * @param {string} key Thread key
 * @returns {Observable<ThreadResponse>} API response for the requested thread
 */
function fetchThread(key) {
  const path = `${apiEndpoint}&id=${key}`;
  /**
   * @type {ClientRequestArgs} options Http request options
   */
  const options = {
    protocol: `${defaultProtocol}:`,
    hostname: defaultHostname,
    port: defaultPort,
    path,
    method: 'GET',
  };

  return new Observable((subscriber$) => httpGet(subscriber$, options));
}

module.exports = {
  fetchThread,
};
