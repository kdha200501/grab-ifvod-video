const path = {
  lock: '.lock.json',
  downloads: 'downloads',
  subscriptions: 'subscriptions',
  subscriptionSample: 'sample.json',
};

const defaultProtocol = 'https';
const defaultHostname = 'm8.ifvod.tv';
const defaultPort = 443;

module.exports = {
  path,
  defaultProtocol,
  defaultHostname,
  defaultPort,
};
