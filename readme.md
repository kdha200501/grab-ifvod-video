## Description

A CLI tool to scrape video streams off of an `Ifvod` thread, it

- saves video streams as `mp4` video files
- groups video files from the same thread into a sub-folder
- remembers downloaded video streams
- provides the option to filter video streams by date, and removes downloaded video files that are out of date



## Prerequisites

This tool utilizes `ffmpeg` to fetch and save `hls` video streams. Make sure `ffmpeg` is installed and it:

• supports `https`

```shell
$ ffmpeg -protocols 2>/dev/null | grep https
```

• supports `x.264`

```shell
$ ffmpeg -codecs 2>/dev/null | grep 264
```

Usually, a `homebrew` install would have these covered. If you're compiling `ffmpeg` yourself, on the Raspberry Pi for example, please read: [compile-and-install-ffmpeg.md](https://bitbucket.org/kdha200501/grab-ifvod-video/src/master/compile-and-install-ffmpeg.md)

Also, make sure the system clock is showing the correct date and time

```shell
$ date
```



## Usage

### Installation

```shell
$ npm i -g grab-ifvod-video
$ mkdir grab-ifvod-video
$ cd grab-ifvod-video
$ grab-ifvod-video -i
```

### Manage threads

```shell
$ nano subscriptions/sample.json
$ touch subscriptions/another-thread.json
```

### Run

```shell
$ grab-ifvod-video -F /path/to/ffmpeg
```

​		note - find out where `ffmpeg` is installed using `$ which ffmpeg`

### Options

```shell
$ grab-ifvod-video -h
Usage: grab-ifvod-video [options]

Options:
  --version                 Show version number                        [boolean]
  -d, --directory           Specify the working directory, defaults to cwd
                                                                        [string]
  -i, --init                Initialize the working directory           [boolean]
  -D, --download-directory  Specify the download directory, defaults to
                            downloads folder under the working directory[string]
  -F, --ffmpeg-bin          Specify the path to ffmpeg binary           [string]
  -q, --quiet               Do not output to stdout or stderr          [boolean]
  -h, --help                Show help                                  [boolean]
```

### Run from crontab

```shell
$ sudo crontab -e
```

​		Insert the following, and remember to change to real schedules and real paths
```
min hr1,hr2 * * * grab-ifvod-video -d /path/to/working/directory -D /path/to/alternative/downloads/directory -F /path/to/ffmpeg >/dev/null 2>&1
```