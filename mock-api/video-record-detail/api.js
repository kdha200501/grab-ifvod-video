const { join } = require('path');

const { readJsonFile } = require('../../utils');

function fetchVideoRecordDetail() {
  return readJsonFile(join(__dirname, 'response.json'));
}

module.exports = {
  fetchVideoRecordDetail,
};
