const { join } = require('path');

const { readJsonFile } = require('../../utils');

function fetchThread() {
  return readJsonFile(join(__dirname, 'response.json'));
}

module.exports = {
  fetchThread,
};
