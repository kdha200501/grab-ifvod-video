# Instructions to installing `ffmpeg` on Raspberry Pi

## 1. Install `x.264`

**Download the source code:**

```shell
$ cd ~/Downloads
$ git clone git://git.videolan.org/x264
$ cd x264
```

**Configure the build:**

```shell
$ ./configure --host=arm-unknown-linux-gnueabi --enable-static --disable-opencl
```

​		note - if the `No working C compiler found` exception is thrown, try `$ sudo aptitude upgrade && sudo aptitude install build-essential`
​		note - if the `relocation R_ARM_MOVW_ABS_NC against 'a local symbol' can not be used when making a shared object; recompile with -fPIC` exception is thrown, try adding the `--enable-pic` option

**Compile:**

```shell
$ make -j4
```

​		note - adjust option to the correct number of CPU cores

**Install:**

```shell
$ sudo make install
```

## 2. Install `ffmpeg`

**Download the source code:**

```shell
$ cd ~/Downloads
$ git clone git://source.ffmpeg.org/ffmpeg.git
$ cd ffmpeg
```

**Configure the build:**

```shell
$ ./configure --enable-openssl --arch=armel --target-os=linux --enable-gpl --enable-libx264 --enable-nonfree
```
​		note - if the `ERROR: openssl not found` exception is thrown, try `$ sudo apt-get install libssl-dev libcurl4-openssl-dev libsasl2-dev pkg-config`

**Compile:**

```shell
$ make -j4
```

​		note - adjust option to the correct number of CPU cores

**Install:**

```shell
$ sudo make install
```


